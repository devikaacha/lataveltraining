<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Designation;

class Team extends Model
{
    use HasFactory;
    public function designation(){
        return $this->belongsTo(Designation::class ,'designation_id');//this will show relationship between designation and team
    }
}
