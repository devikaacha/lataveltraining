<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //Index
    public function index(){
        return view ('admin.service.index');
    }

    //add 
    public function add(){
        return view('admin.service.add');
    }
}
