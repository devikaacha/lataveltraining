<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pricing;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class PricingController extends Controller
{
    //Index Page
    public function index(){
      $pricings=Pricing::all();
      return view('admin.price.index',compact('pricings'));
    }
    //add
    public function add(){
        return view('admin.price.add');
    }
    //store
    public function store(Request $request){
        $data=$request->all();
        
        $rules =[
            'title' =>'required|max:255',
            'features' =>'required',
            'image' =>'required',
            'price' =>'required',

        ];
        $customMessages =[
            'title.required' => 'title is required',
            'features.required' => 'Banner content is required',
            'image.required' => 'image is required',
            'price.required' => 'price is required',
            'title.max' => 'You are not allowed to enter more than 255 characters',
        ];
        $this->validate($request, $rules, $customMessages);
         $pricing=new Pricing();
         $pricing->title=$data['title'];
         $pricing->price=$data['price'];

         $random=str::random(10);
         if($request->hasFile('image')){
             $image_tmp= $request->file('image');
             if($image_tmp->isValid()){
                 $extension = $image_tmp->getClientOriginalExtension();
                 $filename = $random .'.'.$extension;
                $image_path = 'public/uploads/price/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $pricing->image= $filename;


             }
         }
         $features[]=$request->features;
         $featuresAll=json_encode($features);
         $pricing->features=$featuresAll;
         $pricing->save();
         Session::flash('success_message', 'price has been added successfully');
         return redirect()->route('pricing.index');
    }


// Edit Page
 public function edit($id){
    $pricing = Pricing::findOrFail($id);
   return view('admin.price.edit', compact('pricing'));
}
 // Update
 public function update(Request $request, $id){
    $data = $request->all();
    $rules = [
        'title' => 'required|max:255',
        'features' => 'required',
        'price' => 'required',
    ];
    $customMessages = [
        'title.required' => 'Pricing Title is required',
        'title.max' => 'You are not allowed to enter more than 255 Characters',
    ];
    $this->validate($request, $rules, $customMessages);
    $pricing = Pricing::findOrFail($id);
    $pricing->title = $data['title'];
    $pricing->price = $data['price'];

    $random = Str::random(10);
    if($request->hasFile('image')){
        $image_tmp = $request->file('image');
        if($image_tmp->isValid()){
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = $random .'.'. $extension;
            $image_path = 'public/uploads/price/' . $filename;
            Image::make($image_tmp)->save($image_path);
            $pricing->image = $filename;
        }
    }

    $features[] = $request->features;
    $featuresAll = json_encode($features);
    $pricing->features = $featuresAll;
    $pricing->save();
    Session::flash('success_message', 'Pricing has been Updated Successfully');
    return redirect()->route('pricing.index');
}

public function delete($id){
  $pricing = Pricing::findOrFail($id);
  $image_path = 'public/uploads/price/';
  if(file_exists($image_path.$pricing->image)){
    unlink($image_path.$pricing->image);
  }
  $pricing->delete();
  Session::flash('success_message', 'pricing has been deleted Successfully');
  return redirect()->route('pricing.index');
}
}
