<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    //Index
    public function index(){
        $categories=Category::orderBy('category_name','ASC')->get();
        return view('admin.cms.category.index',compact('categories'));
    }
    //store
    public function store(Request $request){
        $data=$request->all();
        $rules =[
            'category_name' =>'required|max:255|unique:categories,category_name',

        ];
        $customMessages =[
            'category_name.required' => 'Category Name is required',
            'category_name.unique' => 'Category Name already exist in database',
            

            
        ];
        $this->validate($request, $rules, $customMessages);
        $category=new Category();
        $category->category_name=$data['category_name'];
        $category->slug=str::slug($data['category_name']);
        $category->save();
        Session::flash('success_message', 'Category has been Added Successfully');
        return redirect()->back();
    }
    //Update
    public function update(Request $request,$id){
        $data=$request->all();
        $category=Category::findOrFail($id);
        $rules =[
            'category_name' =>'required|max:255|unique:categories,category_name,'.$category->id,

        ];
        $customMessages =[
            'category_name.required' => 'Category Name is required',
            'category_name.unique' => 'Category Name already exist in database',
            

            
        ];
        $this->validate($request, $rules, $customMessages);
        
        $category->category_name=$data['category_name'];
        $category->slug=str::slug($data['category_name']);
        $category->save();
        Session::flash('success_message', 'Category has been updated Successfully');
        return redirect()->back();
    }
    //delete
    public function delete($id){
        $category=Category::findOrFail($id);
        $category->delete();
        Session::flash('success_message', 'Category has been deleted Successfully');
        return redirect()->back();
    }
}
