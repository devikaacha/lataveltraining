<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUsPage;
use Illuminate\Support\Facades\Session;
class ContactController extends Controller{
    //Contact Us Page
public function contact(){
    $contact=ContactUsPage::first();
    return view('admin.pages.contact',compact('contact'));
}

// Update Contact Us Page 
public function contactUpdate(Request $request,$id){
    $contact=ContactUsPage::findOrFail($id);
    $data=$request->all();

    $rules =[
        'name' =>'required',
        'email' =>'required',
        'subject' =>'required',
        'message' =>'required',
        

    ];
    $customMessages =[
        'name.required' => 'name is required',
        'email.required' => 'Page title is required',
        'subject.required' => 'Page subtitle is required',
        'message.required' => 'Page content is required',
        //'page_name.max' => 'You are not allowed to enter more than 20 characters',
    ];
    $this->validate($request, $rules, $customMessages);
    $contact->name=$data['name'];
    $contact->email=$data['email'];
    $contact->subject=$data['subject'];
    $contact->message=$data['message'];

    $contact->save();
    Session::flash('success_message', ' Contact page has been updated successfully');
    return redirect()->back();
}
}
