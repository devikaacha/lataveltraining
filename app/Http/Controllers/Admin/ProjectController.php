<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
class ProjectController extends Controller
{
    //Index Page
    public function index(){
        $projects=Project::latest()->get();
        return view('admin.project.index',compact('projects'));
    }
    //add page
    public function add(){
        return view('admin.project.add');
    }

    //store
    public function store(Request $request){
        $data=$request->all();
            $rules =[
                'name' =>'required|max:255',
                'details' =>'required',
                'image' =>'required',   
    
            ];
            $customMessages =[
                'name.required' => 'name is required',
                'details.required' => 'Projedt Details is required',
                'image' => ' Project image is required',
                'name.max' => 'You are not allowed to enter more than 255 characters',
            ];
            $this->validate($request, $rules, $customMessages);
            $project= new Project();
            $project->name=$data['name'];
            $project->slug=Str::slug($data['name']);
            $project->details=$data['details'];

            $random=str::random(10);
         if($request->hasFile('image')){
             $image_tmp= $request->file('image');
             if($image_tmp->isValid()){
                 $extension = $image_tmp->getClientOriginalExtension();
                 $filename = $random .'.'.$extension;
                $image_path = 'public/uploads/project/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $project->image= $filename;
             }
         }
         $project->save();
         Session::flash('success_message', 'Project has been added successfully');
         return redirect()->route('project.index');
    }
    //edit page
    public function edit($id){
        $project=Project::where('id',$id)->first();
        return view('admin.project.edit',compact('project'));
    }
     //Update
    public function update(Request $request,$id){
        $data=$request->all();
            $rules =[
                'name' =>'required|max:255',
                'details' =>'required',
                
    
            ];
            $customMessages =[
                'name.required' => 'name is required',
                'details.required' => 'Projedt Details is required',
                'name.max' => 'You are not allowed to enter more than 255 characters',
            ];
            $this->validate($request, $rules, $customMessages);
            $project=Project::where('id',$id)->first();
            $project->name=$data['name'];
            $project->slug=Str::slug($data['name']);
            $project->details=$data['details'];

            $random=str::random(10);
         if($request->hasFile('image')){
             $image_tmp= $request->file('image');
             if($image_tmp->isValid()){
                 $extension = $image_tmp->getClientOriginalExtension();
                 $filename = $random .'.'.$extension;
                $image_path = 'public/uploads/project/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $project->image= $filename;
             }
         }
         $project->save();
         Session::flash('success_message', 'Project has been updated successfully');
         return redirect()->route('project.index');
    }

    public function delete($id){
        $project=Project::findOrFail($id);
        $project->delete();
        $image_path = 'public/uploads/project/';
        if(file_exists($image_path.$project->image)){
            unlink($image_path.$project->image);
        }
        Session::flash('success_message','Project has been deleted successfully');
          return redirect()->route('project.index');
    }

}
