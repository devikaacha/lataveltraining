<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    //
    // Index Page
    public function index()
    {
        $blogs = Blog::latest()->get();
        return view('admin.cms.index', compact('blogs'));
    }

    // Add Page
    public function add()
    {
        $categories = Category::orderBy('category_name', 'ASC')->get();
        $tags = Tag::orderBy('tag_name', 'ASC')->get();
        return view('admin.cms.add', compact('categories', 'tags'));
    }

    // Store Blog
    public function store(Request $request)
    {
        $data = $request->all();

        $current_user = Auth::guard('admin')->user()->id;

        $rules = [
            'blog_title' => 'required|max:255',
            'blog_content' => 'required',
            'image' => 'required',
            'category_id' => 'required',
        ];
        $customMessages = [
            'blog_title.required' => 'Blog Title is required',
            'blog_content.required' => 'Blog Content is required',
            'blog_title.max' => 'You are not allowed to enter more than 255 Characters',
            'image.required' => 'Please Upload an image',
            'category_id' => 'Please Select an Category'
        ];
        $this->validate($request, $rules, $customMessages);
        $blog = new Blog();
        $blog->blog_title = $data['blog_title'];
        $blog->slug = Str::slug($data['blog_title']);
        $blog->category_id = $data['category_id'];
        $blog->blog_content = $data['blog_content'];

        if (!empty($data['status'])) {
            $blog->status = 'published';
        } else {
            $blog->status = 'draft';
        }

        $blog->admin_id = $current_user;

        $random = Str::random(10);
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/blog/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $blog->image = $filename;
            }
        }

        $tags = $data['tag_id'];

        $blog->save();
        $blog->tags()->attach($tags);
        Session::flash('success_message', 'Blog has been Added Successfully');
        return redirect()->route('blog.index');
    }

    // Add Page
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        $categories = Category::orderBy('category_name', 'ASC')->get();
        $tags = Tag::orderBy('tag_name', 'ASC')->get();
        $blog_tag = $blog->tags()->pluck('tag_id')->toArray();
        return view('admin.cms.edit', compact('categories', 'blog', 'tags', 'blog_tag'));
    }


    // Update Blog
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $current_user = Auth::guard('admin')->user()->id;

        $rules = [
            'blog_title' => 'required|max:255',
            'blog_content' => 'required',
            'category_id' => 'required',
        ];
        $customMessages = [
            'blog_title.required' => 'Blog Title is required',
            'blog_content.required' => 'Blog Content is required',
            'blog_title.max' => 'You are not allowed to enter more than 255 Characters',
            'category_id' => 'Please Select an Category'
        ];
        $this->validate($request, $rules, $customMessages);
        $blog = Blog::findOrFail($id);

        $blog->blog_title = $data['blog_title'];
        $blog->slug = Str::slug($data['blog_title']);
        $blog->category_id = $data['category_id'];
        $blog->blog_content = $data['blog_content'];

        if (!empty($data['status'])) {
            $blog->status = 'published';
        } else {
            $blog->status = 'draft';
        }

        $blog->admin_id = $current_user;

        $random = Str::random(10);
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'public/uploads/blog/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $blog->image = $filename;
            }
        }
        $tags = $data['tag_id'];
        $blog->save();
        $blog->tags()->sync($tags);
        Session::flash('success_message', 'Blog has been Updated Successfully');
        return redirect()->route('blog.index');

    }


    public function delete($id){
        $blog = Blog::findOrFail($id);
        $blog->delete();
        $image_path = 'public/uploads/blog/';
        if(file_exists($image_path.$blog->image)){
            unlink($image_path.$blog->image);
        }
        Session::flash('success_message', 'Blog has been Deleted Successfully');
        return redirect()->route('blog.index');
    }

}



