<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class TagController extends Controller
{
    //Index
    public function index(){
        $tags=Tag::orderBy('tag_name','ASC')->get();
        return view('admin.cms.tag.index',compact('tags'));
    }
    //store
    public function store(Request $request){
        $data=$request->all();
        $rules =[
            'tag_name' =>'required|max:255|unique:tags,tag_name',

        ];
        $customMessages =[
            'tag_name.required' => 'tag Name is required',
            'tag_name.unique' => 'tag Name already exist in database',
            

            
        ];
        $this->validate($request, $rules, $customMessages);
        $tag=new tag();
        $tag->tag_name=strtolower ($data['tag_name']);
        $tag->slug=str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'tag has been Added Successfully');
        return redirect()->back();
    }
    //Update
    public function update(Request $request,$id){
        $data=$request->all();
        $tag=Tag::findOrFail($id);
        $rules =[
            'tag_name' =>'required|max:255|unique:tags,tag_name,'.$tag->id,

        ];
        $customMessages =[
            'tag_name.required' => 'tag Name is required',
            'tag_name.unique' => 'tag Name already exist in database',
            

            
        ];
        $this->validate($request, $rules, $customMessages);
        $tag->tag_name=$data['tag_name'];
        $tag->slug=str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'tag has been updated Successfully');
        return redirect()->back();
    }
    //delete
    public function delete($id){
        $tag=tag::findOrFail($id);
        $tag->delete();
        Session::flash('success_message', 'tag has been deleted Successfully');
        return redirect()->back();
    }
}
