<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Designation;
use Illuminate\Support\Facades\Session;

class DesignationController extends Controller
{
    //Index page
    public function index(){
        $designations=Designation::orderBy('title','ASC')->get();
        return view('admin.team.designation.index',compact('designations'));
    }

    //Store Designation
    public function store(Request $request){
        $data=$request->all();
        $designationCount=Designation::where('title',$data['title'])->count();
        if($designationCount>0){
            return redirect()->back()->with('error_message','Designation name already exist in our database');
        }
        $rules =[
            'title' =>'required|max:255',

        ];
        $customMessages =[
            'title.required' => 'title is required',
            
        ];
        $this->validate($request, $rules, $customMessages);
        $designation=new Designation();
        $designation->title=$data['title'];
        $designation->save();

        Session::flash('success_message','Designation has been added successfully');
          return redirect()->back();

    }

    
     // Update Designation
     public function update(Request $request, $id){
        $data = $request->all();
        $designation = Designation::findOrFail($id);
        $designationCount = Designation::where('id', '!=', $designation->id)->where('title', $data['title'])->count();
        if($designationCount > 0){
            return redirect()->back()->with('error_message', 'Designation Name Already Exists in Our Database');
        }

        $rules = [
            'title' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Banner Title is required',
        ];
        $this->validate($request, $rules, $customMessages);
        $designation->title = $data['title'];
        $designation->save();
        Session::flash('success_message', 'Designation has been Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $designation = Designation::findOrFail($id);
        $designation->delete();
        Session::flash('success_message', 'Designation has been deleted Successfully');
        return redirect()->back();
    }
}
