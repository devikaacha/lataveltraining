<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\AboutUsPage;



class IndexController extends Controller
{
    //Index Page
    public function index(){
        $banners=Banner::orderby('priority','DESC')->get();
        $bannerImage=Banner::orderby('priority','DESC')->first();
        $about=AboutUsPage::first();
        return view('front.index',compact('banners','bannerImage','about'));

    }
}
