<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUsPage;
use App\Models\ContactUsPage;
use App\Models\Team;
use App\Models\Blog;
use App\Models\Pricing;
use App\Models\Project;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Support\Facades\Session;
use App\Models\Testimonial;

class FrontEndController extends Controller
{
    //About Page
    public function aboutUs(){
        $about=AboutUsPage::first();
        $teams=Team::orderBy('priority','ASC')->get();
        return view('front.pages.about',compact('about','teams'));
    }

    //Contact Page
    public function contactUs(){
        $contact=ContactUsPage::first();
        return view('front.pages.contact',compact('contact'));
    }

    //Testimonial
    public function testimonial(){
        $testimonials=Testimonial::latest()->get();
        return view('front.pages.testimonial',compact('testimonials'));
    }

    //Blog
    public function blog(){
        $blogs=Blog::where('status','published')->latest()->paginate(6);
        return view('front.blog.blog',compact('blogs'));
    }

    //Blog Detail
    public function blogDetail($slug){
        $blog=Blog::where('slug', $slug)->first();
        $blogkey='blog_'.$blog->id;
        if(!Session::has($blogkey)){
            $blog->increment('view_count');
            Session::put($blogkey,1);
        }
        $related_post=Blog::where('id','!=',$blog->id)->where('category_id',$blog->category_id)->latest()->take(2)->get();
        $recent_articles=Blog::where('id','!=',$blog->id)->latest()->take(4)->get();
        $categories =Category::orderBy('category_name','ASC')->get();
        $tags= Tag::all();
        return view('front.blog.blogDetail',compact('blog','related_post','recent_articles','categories','tags'));

    }
    //Service
    public function service(){
        return view('front.service.service');
    }

    public function categoryBlog($slug){
        $categoryDetail=Category::where('slug',$slug)->first();
        $blogs=Blog::where('status','published')->where('category_id',$categoryDetail->id)->latest()->paginate(6);
        return view('front.blog.categoryBlog',compact('categoryDetail','blogs'));
        
    }
    //Pricing
    public function pricing(){
        $pricings=Pricing::latest()->get();
        return view('front.pages.pricing',compact('pricings'));
    }

    //Project
    public function project(){
        $projects=Project::latest()->get();
        return view('front.pages.project',compact('projects'));
    }

    
}
