<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use App\Models\SiteSetting;
use App\Models\Social;
use App\Models\AboutUsPage;
use App\Models\ContactUsPage;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Admin::insert([
         'name' => 'Devika Acharya',
         'email' => 'joniacharya81@gmail.com',
         'password' => bcrypt('password'),

      ]);

      Admin::insert([
          'name' => 'Manish Gautam',
          'email' => 'manish@gmail.com',
          'password' => bcrypt('password'),

       ]);

       Theme::insert([
          'website_name'=>'Tech Coderz',
          'website_tagline'=>'Inspire the Next',
          'favicon'=>''
       ]);

         SiteSetting::insert([
            'email'=>'info@project.com'
         ]);

         Social::insert([
            'facebook'=>''
         ]);

         //About Us
         AboutUsPage::insert([
            'page_name'=>'About Us',
            'page_title'=> 'Manages IT Service Across Various Business',
             'page_subtitle'=>'',
             'page_content'=>''
         ]);
         
         //Contact Us
         ContactUsPage::insert([
            'name'=>'Contact Us',
            'email'=> 'info@project.com',
             'subject'=>'Information Technology',
             'message'=>''
         ]);
    }
}
