@extends('front.includes.front_design')
@section('site_title')
 Service Page - {{$theme->website_name}} - {{$theme->website_tagline}}

@endsection

@section('content')
<!-- start page title area-->
<div class="page-title-area bg-thin">
            <div class="container">
                <div class="page-title-content">
                    <h1>services</h1>
                    <ul>
                        <li class="item"><a href="{{route('index')}}">Home</a></li>
                        <li class="item"><a href="pricing.html">services</a></li>
                    </ul>
                </div>
            </div>
            <div class="shape">
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
                <span class="shape4"></span>
            </div>
        </div>
        <!-- end page title area -->

        <!--  start feature section -->
        <section class="feature-section single pt-100 bg-thin">
            <div class="container">
                <div class="section-title">
                    <h2>Let’s Check Out The Features</h2>
                    <p>Does any industry face a more complex audience journey and marketing sales process than B2B technology.Does any industry face a more complex audience.</p>
                </div>
                <div class="row pt-70">
                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-magnify-glass2"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b1.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="service-details.html" target="_self">market research</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="service-details.html" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-code"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b2.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="service-details.html" target="_self">digital Marketing</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="service-details.html" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-mic"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b3.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="service-details.html" target="_self">Social Media</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="service-details.html" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-call-center"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b4.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="service-details.html" target="_self">IT consalting</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="service-details.html" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-server2"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b5.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="service-details.html" target="_self">SEO & Backlinks</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="service-details.html" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-12 pb-70">
                        <div class="item-single">
                            <div class="icon">
                                <i class="envy envy-server"></i>
                                <div class="icon-bg">
                                    <img src="assets/img/resource/icon_shape_b6.png" alt="icon_shape" />
                                </div>
                            </div>
                            <div class="item-content">
                                <h6><a href="#">Hosting & Email</a></h6>
                                <p>Strategy experience and analytical expertise combine to enable. Strate gy experience and ana.</p>
                                <a href="#" target="_self" class="btn btn-text-only">read more <i class="envy envy-right-arrow"></i></a>
                            </div>
                        </div>
                        <!-- item-single -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end feature section -->

         <!-- start works section -->
         <section class="works-section ptb-100 bg-white">
            <div class="container">
                <div class="section-title">
                    <h2>How its work</h2>
                    <p>Does any industry face a more complex audience journey and marketing sales process than B2B technology.Does any industry face a more complex audience.</p>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="work-single">
                            <div class="work-single-content">
                                <i class="envy envy-cloud-computing1"></i>
                                <h4>market research</h4>
                                <div class="index"><span>1</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="work-single">
                            <div class="work-single-content">
                                <i class="envy envy-code2"></i>
                                <h4>research project</h4>
                                <div class="index"><span>2</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="work-single">
                            <div class="work-single-content">
                                <i class="envy envy-global2"></i>
                                <h4>Sketch & Wireframes</h4>
                                <div class="index"><span>3</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="work-single">
                            <div class="work-single-content">
                                <i class="envy envy-server"></i>
                                <h4>data analysis</h4>
                                <div class="index"><span>4</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end works section -->

        <!--start tips section-->
        <section class="tips-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-12 ptb-100">
                        <div class="section-title">
                            <h2>Get Tips And Tricks To Make Best Use The Product</h2>
                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC,</p>
                        </div>
                        <div class="tips-list">
                            <ul>
                                <li>Strategy experience and analytical expertise combine.</li>
                                <li>Good experience and analytical expertise combine. .</li>
                                <li>Simple experience and analytical expertise combine.</li>
                                <li>The standard chunk of Lorem Ipsum used since</li>
                                <li>Strategy experience and analytical expertise combine. </li>
                            </ul>
                        </div>
                        <div class="cta-btn">
                            <a href="#" class="btn btn-solid">start trial <i class="envy envy-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 align-self-end">
                        <div class="tips-img">
                            <img src="assets/img/tips_left.png" alt="tips" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end tips section-->

        <!-- start footer area -->
        <footer class="footer-area pt-100 pb-70 bg-thin">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-5 col-sm-6 col-12">
                        <div class="footer-widget">
                            <div class="navbar-brand">
                                <a href="index.html">
                                    <img src="assets/img/logos/logo_dark.png" alt="image" />
                                </a>
                            </div>
                            <p>Grursus mal suada faci Lorem to the ipsum dolarorit more ame tion more consectetu.</p>
                            <div class="social-link">
                                <a href="#" class="bg-tertiary" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                <a href="#" class="bg-success" target="_blank"><i class="fab fa-tumblr"></i></a>
                                <a href="#" class="bg-danger" target="_blank"><i class="fab fa-youtube"></i></a>
                                <a href="#" class="bg-info" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                <a href="#" class="bg-pink" target="_blank"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-6 col-12">
                        <div class="footer-widget">
                            <h5>Our Services</h5>
                            <ul class="footer-quick-links">
                                <li><a href="service-details.html">Interface Design</a></li>
                                <li><a href="service-details.html">Seo Optimizer</a></li>
                                <li><a href="service-details.html">Digital Marketing</a></li>
                                <li><a href="service-details.html">Market Monitor</a></li>
                                <li><a href="service-details.html">Graphic Design</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-6 col-12">
                        <div class="footer-widget">
                            <h5>Other Resources</h5>
                            <ul class="footer-quick-links">
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="contact.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-6 col-12">
                        <div class="footer-widget">
                            <h5>Contact info</h5>
                            <div class="contact-info">
                                <i class="envy envy-pin"></i>
                                <p>113 Solit, White House, New Jersey, USA</p>
                            </div>
                            <div class="contact-info">
                                <i class="envy envy-call"></i>
                                <p>
                                    <a href="tel:+001-548-159-2491">+001-548-159-2491</a>
                                </p>
                                <p>
                                    <a href="tel:+001-548-159-2492">+001-548-159-2492</a>
                                </p>
                            </div>
                            <div class="contact-info">
                                <i class="envy envy-plane"></i>
                                <p>
                                    <a href="mailTo:hello@solit.com">hello@solit.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end footer area -->

        <!-- start copy right area -->
        <div class="copy-right-area">
            <div class="container">
                <div class="copy-right-content">
                    <p>
                        Copyright @2021 Solit. Designed By
                        <a href="https://hibootstrap.com/" target="_blank">
                            HiBootstrap.com
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <!-- end copy right area-->
