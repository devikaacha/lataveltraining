@extends('front.includes.front_design')
@section('site_title')
 ContactUsPage - {{$theme->website_name}} - {{$theme->website_tagline}}

@endsection

@section('content')
<!-- start page title area-->
<div class="page-title-area bg-thin">
            <div class="container">
                <div class="page-title-content">
                    <h1>Contact</h1>
                    <ul>
                        <li class="item"><a href="{{route('index')}}">Home</a></li>
                        <li class="item"><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="shape">
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
                <span class="shape4"></span>
            </div>
        </div>
        <!-- end page title area -->
        <section class="contact-section ptb-100 bg-thin">
            <div class="container">
                <div class="section-title">
                    <h2>Get In Touch</h2>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-sm-12">
                        <form id="contactForm">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" id="name" required="" data-error="Please enter your name" placeholder="Name" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" id="email" required="" data-error="Please enter your email" placeholder="Email" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="subject" class="form-control" id="subject" required="" data-error="Please enter your phone number" placeholder="Subject" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <textarea name="message" id="message" class="form-control" cols="30" rows="6" required="" data-error="Please enter your message" placeholder="Message..."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="btn btn-solid">send message</button>
                                    <div id="msgSubmit" class="h5 text-center hidden"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <div class="contact-widget">
                            <div class="info-title">
                                <h5>contact info</h5>
                            </div>
                            <div class="contact-info">
                                <i class="envy envy-pin"></i>
                                <p>113 Solit, White House, New Jersey, USA</p>
                            </div>
                            <div class="contact-info">
                                <i class="envy envy-call"></i>
                                <p>
                                    <a href="tel:+001-548-159-2491">+001-548-159-2491</a>
                                </p>
                                <br>
                                <p>
                                    <a href="tel:+001-548-159-2492">+001-548-159-2492</a>
                                </p>
                            </div>
                            <div class="contact-info">
                                <i class="envy envy-plane"></i>
                                <p>
                                    <a href="mailTo:hello@solit.com">hello@solit.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="contact-map">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2324.260770535203!2d-76.19672323844269!3d38.95788674975201!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b8119b945a651d%3A0x1c978bc96f4af380!2s113%20Whitehouse%20Dr%2C%20Grasonville%2C%20MD%2021638%2C%20USA!5e0!3m2!1sen!2sbd!4v1601091256726!5m2!1sen!2sbd"
                            ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>


@endsection