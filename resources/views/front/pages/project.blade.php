@extends('front.includes.front_design')
@section('site_title')
 Projects- {{$theme->website_name}} - {{$theme->website_tagline}}

@endsection

@section('content')
<!-- start page title area-->
        <div class="page-title-area bg-thin">
            <div class="container">
                <div class="page-title-content">
                    <h1>Projects</h1>
                    <ul>
                        <li class="item"><a href="{{route('index')}}">Home</a></li>
                        <li class="item"><a href="projects.html">Projects</a></li>
                    </ul>
                </div>
            </div>
            <div class="shape">
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
                <span class="shape4"></span>
            </div>
        </div>
        <!-- end page title area -->

        <!-- start gallery section -->
        <section class="gallery-section ptb-100 bg-white">
            <div class="container">
                <div class="section-title">
                @foreach($projects as $project)
                </div>
                <div class="gallery-slider owl-carousel">
                    <div class="gallery-item">
                        <div class="gallery-image"><img src="{{asset('public/uploads/project/'.$project->image)}}" alt="gallery-member" /></div>
                        <div class="gallery-content">
                            <h3>
                                <a href="project-details.html">UI/UX Design</a>
                            </h3>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- end gallery section -->

        <!-- start client section  -->
        <section class="client-section testimonial ptb-100">
            <div class="container">
                <div class="section-title pb-5">
                    <span class="subtitle">TESTIMONIALS</span>
                    <h2>What People Say About Us</h2>
                    <p>Does any industry face a more complex audience journey and marketing sales process than B2B technology.Does any industry face a more complex audience.</p>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="clients-img">
                            <img src="{{asset('public/frontend/assets/img/clients/client_1.png')}}" alt="client-1" />
                            <img src="{{asset('public/frontend/assets/img/clients/client_2.png')}}" alt="client-2" />
                            <img src="{{asset('public/frontend/assets/img/clients/client_3.png')}}" alt="client-3" />
                            <img src="{{asset('public/frontend/assets/img/clients/client_4.png')}}" alt="client-4" />
                            <img src="{{asset('public/frontend/assets/img/clients/client_5.png')}}" alt="client-5" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="client-slider owl-carousel">

                            <div class="slider-item">
                                <div class="qoute-icon">
                                    <img src="{{asset('public/frontend/assets/img/resource/quotation.png')}}" alt="" />
                                </div>
                                <div class="inner-text">
                                    <p>
                                    If you are looking for the rewarding career and the chance to make an impact, you have come to the right place. We will transform your business through our techniques.
                                    </p>
                                </div>
                                <!-- slider Image -->
                                <div class="client">
                                    <div class="client-img">
                                        <img src="{{asset('public/frontend/assets/img/clients/client_7.png')}}" alt="client-1" />
                                    </div>
                                    <div class="client-info">
                                        <h6>David McLean</h6>
                                        <span>CEO & Manager</span>
                                    </div>
                                </div>
                            </div>
                            <!-- slider item -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="shape">
                <img src="{{asset('public/frontend/assets/img/resource/shape_6.png')}}" alt="shape" class="shape-inner" />
                <img src="{{asset('public/frontend/assets/img/resource/Ellipse_1.png')}}" alt="shape" class="shape-inner" />
                <img src="{{asset('public/frontend/assets/img/resource/Rectangle_1.png')}}" alt="shape" class="shape-inner" />
                <img src="{{asset('public/frontend/assets/img/resource/Ellipse_1.png')}}" alt="shape" class="shape-inner" />
                <img src="{{asset('public/frontend/assets/img/resource/Rectangle_1.png')}}" alt="shape" class="shape-inner" />
                <img src="{{asset('public/frontend/assets/img/resource/shape_2.png')}}" alt="shape" class="shape-inner" />
            </div>
        </section>
        <!-- end client section  -->
        @endsection
