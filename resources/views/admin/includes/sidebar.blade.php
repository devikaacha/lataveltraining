<!--start sidebar -->
<aside class="sidebar-wrapper" data-simplebar="true">
          <div class="sidebar-header">
            <div>
              <img src="{{asset('public/adminpanel/assets/images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
            </div>
            <div>
              <h4 class="logo-text">Onedash</h4>
            </div>
            <div class="toggle-icon ms-auto"> <i class="bi bi-list"></i>
            </div>
          </div>
          <!--navigation-->
          <ul class="metismenu" id="menu">
            <li>
              <a href="{{ route('adminDashboard') }}">
                <div class="parent-icon"><i class="bi bi-house-fill"></i>
                </div>
                <div class="menu-title">Dashboard</div>
              </a>
              
            </li>

            <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="lni lni-users"></i>
                </div>
                <div class="menu-title">Teams</div>
              </a>
              <ul>
                <li> <a href="{{ route('designation.index') }}"><i class="bi bi-circle"></i>Designations</a>
                </li>
                <li> <a href="{{route('team.index')}}"><i class="bi bi-circle"></i>All Teams</a>
                </li>
                
              </ul>
            </li>

            <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="bx bx-shape-polygon"></i>
                </div>
                <div class="menu-title">Services</div>
              </a>
              <ul>
                <li> <a href="{{ route('service.index') }}"><i class="bi bi-circle"></i>Category</a>
                </li>
                <li> <a href="{{route('tag.index')}}"><i class="bi bi-circle"></i>Tags</a>
                </li>
                <li> <a href="{{route('blog.index')}}"><i class="bi bi-circle"></i>Blogs</a>
                </li>
              </ul>
            </li>

            <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="bx bx-note"></i>
                </div>
                <div class="menu-title">CMS</div>
              </a>
              <ul>
                <li> <a href="{{ route('category.index') }}"><i class="bi bi-circle"></i>Category</a>
                </li>
                <li> <a href="{{route('tag.index')}}"><i class="bi bi-circle"></i>Tags</a>
                </li>
                <li> <a href="{{route('blog.index')}}"><i class="bi bi-circle"></i>Blogs</a>
                </li>
              </ul>
            </li>


            <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="lni lni-cog"></i>
                </div>
                <div class="menu-title">Seetings</div>
              </a>
              <ul>
                <li> <a href="{{ route('theme') }}"><i class="bi bi-circle"></i>Theme Settings</a>
                </li>
                <li> <a href="{{route('settings')}}"><i class="bi bi-circle"></i>Site Settings</a>
                </li>
                <li> <a href="{{route('social')}}"><i class="bi bi-circle"></i>Social Media Settings</a>
                </li>
              </ul>
            </li>

           

            <li>
              <a href="{{ route('banner.index') }}">
                <div class="parent-icon"><i class="bi bi-image"></i>
                </div>
                <div class="menu-title">Banner Management</div>
              </a>
              
            </li>

            <li>
              <a href="{{ route('pricing.index') }}">
                <div class="parent-icon"><i class="bx bx-money"></i>
                </div>
                <div class="menu-title">Pricings</div>
              </a>
              
            </li>

            <li>
              <a href="{{ route('project.index') }}">
                <div class="parent-icon"><i class="bx bx-grid"></i>
                </div>
                <div class="menu-title">Projects</div>
              </a>
              
            </li>

            <li>
              <a href="{{ route('testimonial.index') }}">
                <div class="parent-icon"><i class="bx bx-comment-detail"></i>
                </div>
                <div class="menu-title">Testimonials</div>
              </a>
              
            </li>


            <li>
              <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class="lni lni-files"></i>
                </div>
                <div class="menu-title">Pages</div>
              </a>
              <ul>
                <li> <a href="{{ route('about') }}"><i class="bi bi-circle"></i>About Us Page</a>
                </li>
                
              </ul>
            </li>


          </ul>
      <!--end navigation-->
       </aside>
       <!--end sidebar -->
