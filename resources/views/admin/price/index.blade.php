@extends('admin.includes.admin_design')

@section('content')
 <!--start content-->
 <main class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Pricing Management</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="{{route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">View ALL Pricings</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">	
							<a href="{{route('pricing.add')}}" class="btn btn-primary"> <i class="bi bi-plus"></i>Add New Pricing</a>	
					</div>
				</div>
				<!--end breadcrumb-->
				
				<hr/>
				@include('admin.includes._message')
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>S.N</th>
										<th>image</th>
										<th>pricing title</th>
										<th>price</th>
										<th>features</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
								
								@foreach($pricings as $price)
								<tr>
									<td>{{$loop->index + 1}}</td>
									<td>
										<img src="{{asset('public/uploads/price/'.$price->image)}}" width="50px" alt="">
									</td>
									<td>{{$price->title}}</td>
									<td>{{$price->price}}</td>
									<?php $resp =  json_decode($price->features) ?>
									<td>
									@for($i = 0; $i < sizeof($resp[0]); $i++)
										<li>{{ $resp[0][$i] }}</li>
										@endfor
									</td>

									<td>
											<a href="{{route('pricing.edit' , $price->id)}}" class="btn btn-sm btn-info" style="color: white">
												<i class="bx bx-edit-alt"></i></a>

												<a href="javascript:" rel="{{ $price->id }}" rel1="delete-pricing" class="btn btn-sm btn-danger btn-delete" style="color: white">
												<i class="bx bx-trash-alt"></i></a>
										
										
										</td>
								</tr>
								@endforeach
                                </tbody>
								
							</table>
						</div>
					</div>
				</div>
				
				
				
			</main>
       <!--end page main-->

@endsection

@section('js') 

<script>
        $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>

	@endsection
